#include <stdio.h>
#include <ncurses.h>
#include <locale.h>
#include <time.h>

// 陣形の位置 1:左上・右下 0:左下・右上
#define LU_FORMATION (1)
// 盤の大きさ
#define SIZE_FIELD (20)
// 正方形陣形の一辺の大きさ
#define SIZE_SIDE (6)
// 片方の兵の総数
#define SIZE_PAWN (SIZE_SIDE*SIZE_SIDE)
// アニメーションのコマ間隔(マイクロ秒)
#define ANIME_DELAY (10000)
// アニメーションをスキップする
#define ANIME_SKIP (1)
// プレイヤーをAIが操作する(デフォルト値)
#define P0_AI (1)
#define P1_AI (1)

// 文章表示用
#define SCRIPT_START \
	"ゲーム開始"
#define SCRIPT_START_M \
	"(何かキーを押して開始)"
#define SCRIPT_SELECT \
	"コマを選んでください"
#define SCRIPT_SELECT_M \
	"(十字:カーソル移動 z:移動 x:攻撃 c:番号選択 e:ターン変更 Tab:未行動機探索 q:終了)"
#define SCRIPT_NUM \
	"[   ] コマを番号で選んでください"
#define SCRIPT_NUM_M \
	"(数字入力->[Enter]:番号選択 その他->[Enter]:キャンセル)"
#define SCRIPT_TURN_CHANGE \
	"ターンを変更しますか？"
#define SCRIPT_TURN_CHANGE_M \
	"(z:変更 x:キャンセル)"
#define SCRIPT_ACT \
	"行動を選んでください"
#define SCRIPT_ACT_M \
	"(z:移動 x:攻撃 c:キャンセル v:行動終了)"
#define SCRIPT_MOVE \
	"移動先を選んでください"
#define SCRIPT_MOVE_M \
	"(十字:カーソル移動 z:決定 x:キャンセル)"
#define SCRIPT_ATTACK \
	"攻撃先を選んでください"
#define SCRIPT_ATTACK_M \
	"(十字:カーソル移動 z:決定 x:キャンセル)"
#define SCRIPT_CANSEL \
	"キャンセルされました"
#define SCRIPT_CANSEL_M \
	""
#define SCRIPT_WIN_P0 \
	"ゲームオーバー 赤軍の勝利"
#define SCRIPT_WIN_P0_M \
	"(r:再戦 q:終了)"
#define SCRIPT_WIN_P1 \
	"ゲームオーバー 青軍の勝利"
#define SCRIPT_WIN_P1_M \
	"(r:再戦 q:終了)"
#define SCRIPT_CPU_OPE \
	"CPUの操作中です"
#define SCRIPT_CPU_OPE_M \
	"( ...しばらくお待ちください)"
#define SCRIPT_AI_END \
	"CPUのターンが終了しました"
#define SCRIPT_AI_END_M \
	"(何かキーを押して自分のターンを開始)"

// 色管理
enum CLR
{
	C_RED = 1,
	C_BLUE,
	C_YELLOW,
};

// ゲームモード管理
enum MODE
{
	MODE_START,
	MODE_SELECT,
	MODE_NUM,
	MODE_TURN,
	MODE_SELECTED,
	MODE_MOVE,
	MODE_ATTACK,
	MODE_GAMEOVER,
	MODE_AI,
	MODE_DEBUG,
};

// 方向管理
enum DIR
{
	DIR_UP,
	DIR_DOWN,
	DIR_LEFT,
	DIR_RIGHT,
};

// キー定数補完
#define KEY_ESC 27
#define KEY_TAB 9

// ゲーム初期化用パラメータ
typedef struct
{
	int p_isAI[2];
	int lim_trial;
} para;

// 兵士(pawn)
typedef struct 
{
	int num;
	int mov;
	int atk;
	int hp;
	int i;
	int j;
} pawn;

// 全ての兵
pawn man[SIZE_PAWN*2];
// 盤から兵を示す
pawn* field[SIZE_FIELD][SIZE_FIELD];
// ある兵の移動可能判定
int movable[7][7];
// デバッグ用の一時保存座標
int y_debug, x_debug;


// -------- ゲームの操作 --------
// ゲーム進行
void game_operator(para);
// ゲーム終了判定
int game_isOver(void);
// 盤面初期化
void field_init(void);
// -------- -------- --------

// -------- 兵の操作 --------
// 兵の初期化
void pawn_init(pawn* x, int n, int i, int j);
// 番号による兵の指定
pawn* pawn_select_num(int n);
// 座標による兵の指定
pawn* pawn_select_grid(int i, int j);
// 兵を座標へ移動
int pawn_move(pawn* x, int i, int j);
// 兵を兵に攻撃
int pawn_attack(pawn* x, pawn* y);
// 兵が兵に攻撃できるか判定
int pawn_isAttackable(pawn* x, pawn* y);
// 兵が座標へ移動できるか判定
int pawn_isMovable(pawn* x, int i, int j, int turn);
// 兵が移動・攻撃可能か判定
int pawn_isAvailable(pawn* x);
// 兵の移動可能範囲をmovableに記録
void pawn_setMovable(pawn* x, int turn);
// 全ての兵の移動終了・攻撃終了を回復
void pawn_activate(void);
// 座標が盤の中か判定
int pawn_isInside(int i, int j);
// -------- -------- --------

// -------- 画面の操作 --------
// 盤の枠を表示
void scr_disp_frame(void);
// 盤の中を空白で埋めて表示
void scr_disp_none(void);
// 操作するプレイヤーを表示
void scr_set_status(int turn);
// 操作ガイド用文章を表示
void scr_set_script(char*, char*);
// 兵の番号を表示
void scr_disp_pawn_num(void);
// 兵を所持者で色分けして表示
void scr_over_owner(void);
// 移動可能マスを色付けして表示
void scr_over_move(pawn* x, int turn);
// 表示関数をテキストカーソルを移動させずに実行
void scr_keep_call(void (*func)(void));
// 数字入力の実行
int scr_get_num(void);
// 連続で画面更新する際に遅延をつけてコマ送りで表示
void scr_animation(void);
// 連続試行時の進捗表示
void scr_disp_progress(int, int);

// マス目単位でカーソルを移動
void grid_move(int i, int j);
// マス目単位で上下左右にカーソルを移動
void grid_move_dir(int dir, int* i, int* j);
// デバッグ用の表示
void debug_print(int i, char* str);
// デバッグ用の表示(数字付き)
void debug_print_num(int i, char* str, int x);
// -------- -------- --------

// -------- AI --------
// 自動実行部分
void ai0_control(int skip_anime);
void ai1_control(int skip_anime);
// 重心獲得
void ai_get_g(int* gi, int* gj);
// 隣接する誰かに攻撃
void ai_attack_any(pawn* x);
// ランダム方向に移動
void ai_rand_move(pawn* x);
// -------- -------- --------


int main(int argc, char const *argv[])
{
	para prm;
	if (argc >= 2)
	{
		prm.p_isAI[0] = argv[1][0] == '0' ? 0 : 1;
		prm.p_isAI[1] = argv[1][1] == '0' ? 0 : 1;
	}
	else
	{
		prm.p_isAI[0] = prm.p_isAI[1] = 1;
	}

	if (argc >= 3)
	{
		prm.lim_trial = atoi(argv[2]);
	}
	else
	{
		prm.lim_trial = 0;
	}

	game_operator(prm);
	return 0;
}

void game_operator(para prm)
{
	int command = -1;
	int mode = MODE_SELECT;
	int turn = 0;
	int y0, x0;
	int y_, x_;
	int i_, j_;
	int roop = 1;
	int roop_get_num = 1;
	int get_num;
	int temp;
	int search = -1;
	int selected;
	int won = -1;
	int p_isAI[2] = {prm.p_isAI[0], prm.p_isAI[1]};
	int skip_anime = ANIME_SKIP;
	int trial = 0;
	int lim_trial = prm.lim_trial;
	int p_win[2] = {0, 0};

	// seed初期化
	srand((unsigned)time(NULL));

	// screen初期化
	setlocale(LC_ALL,"ja_JP.UTF-8");
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	// 色セット初期化
	start_color();
	init_pair(C_RED, COLOR_WHITE, COLOR_RED);
	init_pair(C_BLUE, COLOR_WHITE, COLOR_BLUE);
	init_pair(C_YELLOW, COLOR_WHITE, COLOR_YELLOW);

	// 盤面初期化
	field_init();

	scr_disp_frame();
	scr_disp_pawn_num();
	scr_over_owner();
	scr_set_status(turn);

	grid_move(i_ = 0, j_ = 0);
	mode = MODE_START;

	while(roop)
	{
		scr_set_status(turn);
		debug_print_num(3, "mode: ", mode);
		debug_print_num(4, "command: ", command);
		debug_print_num(5, "turn:", turn);
		debug_print_num(6, "trial:", trial);

		switch (mode)
		{
		case MODE_START:
		{
			scr_set_script(SCRIPT_START, SCRIPT_START_M);
			if (lim_trial == 0)
			{
				switch (command = getch())
				{
				default:
					field_init();
					scr_disp_frame();
					scr_disp_pawn_num();
					scr_over_owner();
					scr_set_status(turn);
					grid_move(i_ = 0, j_ = 0);
					turn = 0;
					mode = p_isAI[0] ? MODE_AI : MODE_SELECT;
					break;
				}
			}
			else if (trial < lim_trial)
			{
				field_init();
				scr_disp_frame();
				scr_disp_pawn_num();
				scr_over_owner();
				scr_set_status(turn);
				grid_move(i_ = 0, j_ = 0);
				turn = 0;
				mode = p_isAI[0] ? MODE_AI : MODE_SELECT;				
			}
			else
			{
				roop = 0;
			}
			break;
		}
		case MODE_SELECT:
		{
			scr_set_script(SCRIPT_SELECT, SCRIPT_SELECT_M);
			switch (command = getch())
			{
			case KEY_UP:
				grid_move_dir(DIR_UP, &i_, &j_);
				break;
			case KEY_DOWN:
				grid_move_dir(DIR_DOWN, &i_, &j_);
				break;
			case KEY_LEFT:
				grid_move_dir(DIR_LEFT, &i_, &j_);
				break;
			case KEY_RIGHT:
				grid_move_dir(DIR_RIGHT, &i_, &j_);
				break;
			case KEY_TAB:
				if (search == -1)
				{
					search = turn%2*SIZE_PAWN;
				}
				else if (search == (turn%2+1)*SIZE_PAWN - 1)
				{
					search = turn%2*SIZE_PAWN;
				}
				else
				{
					search++;
				}
				
				for (; search < (turn%2+1)*SIZE_PAWN; ++search)
				{
					if (pawn_isAvailable(&man[search]))
					{
						grid_move(i_ = man[search].i, j_ = man[search].j);
						break;
					}
					if (search == (turn%2+1)*SIZE_PAWN - 1)
					{
						search = turn%2*SIZE_PAWN-1;
					}
				}
				break;
			case 'e':
				mode = MODE_TURN;
				break;
			case 'z':
				if (pawn_isAvailable(field[i_][j_]))
				{
					selected = field[i_][j_]->num;
					if (man[selected].mov == 0)
					{
						mode = MODE_MOVE;
					}
					// mode = MODE_SELECTED;
				}
				break;
			case 'x':
				if (pawn_isAvailable(field[i_][j_]))
				{
					selected = field[i_][j_]->num;
					if (man[selected].atk == 0)
					{
						mode = MODE_ATTACK;
					}
					// mode = MODE_SELECTED;
				}
				break;
			case 'c':
				mode = MODE_NUM;
				break;
			case 'q':
				roop = 0;
				break;
			default:
				break;
			}
			if (command != KEY_TAB)
			{
				search = -1;
			}
			break;
		}
		case MODE_NUM:
		{
			scr_set_script(SCRIPT_NUM, SCRIPT_NUM_M);
			getyx(stdscr, y_, x_);
		
			get_num = scr_get_num();
			if (0 <= get_num && get_num < SIZE_PAWN*2)
			{
				if (pawn_isAvailable(&man[get_num]))
				{
					grid_move(i_ = man[get_num].i, j_ = man[get_num].j);
					selected = get_num;
					mode = MODE_SELECTED;
				}
				else
				{
					scr_set_script(SCRIPT_CANSEL, SCRIPT_CANSEL_M);
					mode = MODE_SELECT;
				}
			}
			else if (get_num == -1)
			{
				scr_set_script(SCRIPT_CANSEL, SCRIPT_CANSEL_M);
				mode = MODE_SELECT;
			}
			else
			{
				scr_set_script(SCRIPT_CANSEL, SCRIPT_CANSEL_M);
				mode = MODE_SELECT;
			}
			break;
		}
		case MODE_TURN:
		{
			if (p_isAI[turn%2])
			{
				turn++;
				pawn_activate();
				if (p_isAI[turn%2])
				{
					if (!skip_anime)
					{
						scr_animation();
					}
					move(25, 2);
					mode = MODE_AI;
				}
				else
				{
					scr_set_script(SCRIPT_SELECT, SCRIPT_SELECT_M);
					scr_disp_pawn_num();
					scr_over_owner();
					grid_move(i_, j_);
					mode = MODE_SELECT;					
				}
			}
			else
			{
				scr_set_script(SCRIPT_TURN_CHANGE, SCRIPT_TURN_CHANGE_M);
				command = getch();
				switch (command)
				{
					case 'z':
					// case 10: // KEY_ENTER
						turn++;
						scr_set_script(SCRIPT_SELECT, SCRIPT_SELECT_M);
						pawn_activate();
						mode = p_isAI[turn%2] ? MODE_AI : MODE_SELECT;
						break;
					case 'x':
						mode = MODE_SELECT;
					default:
						break;
				}
			}
			break;
		}
		case MODE_SELECTED:
		{
			scr_set_script(SCRIPT_ACT, SCRIPT_ACT_M);
			switch (command = getch())
			{
			case 'z':
			{
				if (man[selected].mov == 0)
				{
					mode = MODE_MOVE;
				}
				break;
			}
			case 'x':
			{
				if (man[selected].atk == 0)
				{
					mode = MODE_ATTACK;				
				}
				break;
			}
			case 'c':
			{
				mode = MODE_SELECT;
				break;
			}
			case 'v':
			{
				man[selected].mov = 1;
				man[selected].atk = 1;
				mode = MODE_SELECTED;
				break;
			}
			case 'q':
			{
				roop = 0;
				break;
			}
			default:
				break;
			}
			break;
		}
		case MODE_MOVE:
		{
			scr_set_script(SCRIPT_MOVE, SCRIPT_MOVE_M);
			pawn_setMovable(&man[selected], turn);
			scr_over_move(&man[selected], turn);
			switch (command = getch())
			{
			case KEY_UP:
				grid_move_dir(DIR_UP, &i_, &j_);
				break;
			case KEY_DOWN:
				grid_move_dir(DIR_DOWN, &i_, &j_);
				break;
			case KEY_LEFT:
				grid_move_dir(DIR_LEFT, &i_, &j_);
				break;
			case KEY_RIGHT:
				grid_move_dir(DIR_RIGHT, &i_, &j_);
				break;
			case 'z':
				if (pawn_isMovable(&man[selected], i_, j_, turn) && field[i_][j_] == NULL)
				{
					pawn_move(&man[selected], i_, j_);
					scr_keep_call(scr_disp_pawn_num);
					scr_keep_call(scr_over_owner);
					mode = MODE_SELECT;
				}
				break;
			case 'x':
				{
					scr_keep_call(scr_disp_pawn_num);
					scr_keep_call(scr_over_owner);
					mode = MODE_SELECT;
					break;
				}
			case 'q':
				roop = 0;
				break;
			default:
				break;
			}
			break;
		}
		case MODE_ATTACK:
		{
			scr_set_script(SCRIPT_ATTACK, SCRIPT_ATTACK_M);
			switch (command = getch())
			{
			case KEY_UP:
				grid_move_dir(DIR_UP, &i_, &j_);
				break;
			case KEY_DOWN:
				grid_move_dir(DIR_DOWN, &i_, &j_);
				break;
			case KEY_LEFT:
				grid_move_dir(DIR_LEFT, &i_, &j_);
				break;
			case KEY_RIGHT:
				grid_move_dir(DIR_RIGHT, &i_, &j_);
				break;
			case 'z':
				if (pawn_isAttackable(&man[selected], field[i_][j_]))
				{
					pawn_attack(&man[selected], field[i_][j_]);
					scr_keep_call(scr_disp_pawn_num);
					scr_keep_call(scr_over_owner);
					if ((won = game_isOver()) == -1)
					{
						mode = MODE_SELECT;
					}
					else
					{
						mode = MODE_GAMEOVER;
					} 
				}
				break;
			case 'x':
				mode = MODE_SELECT;
				break;
			case 'q':
				roop = 0;
				break;
			default:
				break;
			}
			break;
		}
		case MODE_GAMEOVER:
		{
			if (won == 0 || won == 1)
			{
				p_win[won]++;
				trial++;
			}

			if (lim_trial == 0)
			{
				scr_keep_call(scr_disp_pawn_num);
				scr_keep_call(scr_over_owner);
				if (won == 0)
				{
					scr_set_script(SCRIPT_WIN_P0, SCRIPT_WIN_P0_M);
				}
				else
				{
					scr_set_script(SCRIPT_WIN_P1, SCRIPT_WIN_P1_M);
				}
				switch (command = getch())
				{
					case 'r':
					{
						mode = MODE_START;
						break;
					}
					default:
						break;
				}
			}
			else if (trial < lim_trial)
			{
				if (p_isAI[0] && p_isAI[1])
				{
					if (trial % 10 == 0)
					{
						scr_disp_progress(trial, lim_trial);
						wrefresh(stdscr);
					}
				}
				else
				{
					if (won == 0)
					{
						scr_set_script(SCRIPT_WIN_P0, SCRIPT_WIN_P0_M);
					}
					else
					{
						scr_set_script(SCRIPT_WIN_P1, SCRIPT_WIN_P1_M);
					}
				}
				mode = MODE_START;
			}
			else
			{
				scr_keep_call(scr_disp_pawn_num);
				scr_keep_call(scr_over_owner);
				roop = 0;
			}
			break;
		}
		case MODE_AI:
		{
			scr_set_script(SCRIPT_CPU_OPE, SCRIPT_CPU_OPE_M);
			// 自動操作ここから

			if (turn%2 == 0)
			{
				ai0_control(skip_anime);
			}
			else
			{
				ai1_control(skip_anime);
			}

			// ここまで
			if (!skip_anime)
			{
				scr_keep_call(scr_disp_pawn_num);
				scr_keep_call(scr_over_owner);
			}
			if ((won = game_isOver()) == -1)
			{
				mode = MODE_TURN;
			}
			else
			{
				mode = MODE_GAMEOVER;
			} 
			// scr_set_script(SCRIPT_AI_END, SCRIPT_AI_END_M);
			// command = getch();
			break;
		}
		case MODE_DEBUG:
		{
			switch (command = getch())
			{
			case 'q':
				roop = 0;
				break;
			default:
				break;
			}
			break;
		}
		default:
			break;
		}

		if (command == KEY_ESC || command == 'q')
		{
			roop = 0;
		}
	}

	endwin();
	if (trial > 0)
	{
		printf("試行数:%d\n"
				"p0勝数:%d\n"
				"p0勝率:%.3f\n", trial, p_win[0], (double)p_win[0] / trial);	
	}
	else
	{
		printf("試行数:0\n");
	}
}
int game_isOver(void)
{
	int n;
	int flag;
	for (flag = 1, n = 0; n < SIZE_PAWN; ++n)
	{
		if (man[n].hp > 0)
		{
			flag = 0;
		}
	}
	if (flag)
	{
		return 1;
	}

	for (flag = 1, n = SIZE_PAWN; n < SIZE_PAWN*2; ++n)
	{
		if (man[n].hp > 0)
		{
			flag = 0;
		}
	}
	if (flag)
	{
		return 0;
	}

	return -1;
}

void field_init(void)
{
	int i, j, n;
	for (i = 0; i < SIZE_FIELD; ++i)
	{
		for (j = 0; j < SIZE_FIELD; ++j)
		{
			field[i][j] = NULL;
		}
	}

	n = 0;
	if (LU_FORMATION)
	{
		for (i = 0; i < SIZE_SIDE; ++i)
		{
			for (j = 0; j < SIZE_SIDE; ++j)
			{
				pawn_init(&man[n], n, i, j);
				field[i][j] = &man[n];
				n++;
			}
		}

		for (i = SIZE_FIELD - SIZE_SIDE; i < SIZE_FIELD; ++i)
		{
			for (j = SIZE_FIELD - SIZE_SIDE; j < SIZE_FIELD; ++j)
			{
				pawn_init(&man[n], n, i, j);
				field[i][j] = &man[n];
				n++;
			}
		}
	}
	else
	{
		for (i = SIZE_FIELD - SIZE_SIDE; i < SIZE_FIELD; ++i)
		{
			for (j = 0; j < SIZE_SIDE; ++j)
			{
				pawn_init(&man[n], n, i, j);
				field[i][j] = &man[n];
				n++;
			}
		}

		for (i = 0; i < SIZE_SIDE; ++i)
		{
			for (j = SIZE_FIELD - SIZE_SIDE; j < SIZE_FIELD; ++j)
			{
				pawn_init(&man[n], n, i, j);
				field[i][j] = &man[n];
				n++;
			}
		}
	}
}

void pawn_init(pawn* x, int num, int i, int j)
{
	x->num = num;
	x->mov = 0;
	x->atk = 0;
	x->hp = 2;
	x->i = i;
	x->j = j;
}
pawn* pawn_select_num(int n)
{
	if (0 <= n && n < SIZE_PAWN*2)
	{
		return &man[n];
	}
	else
	{
		return NULL;
	}
}
pawn* pawn_select_grid(int i, int j)
{
	int n;
	if (i < 0 || SIZE_FIELD <= i)
	{
		return NULL;
	}

	if (j < 0 || SIZE_FIELD <= j)
	{
		return NULL;
	}

	// field参照有り
	return field[i][j];

	// field参照無し
	// for (n = 0; n < SIZE_PAWN*2; ++n)
	// {
	// 	if (man[n].i == i && man[n].j == j)
	// 	{
	// 		return &man[n];
	// 	}
	// }

	return NULL;
}
int pawn_move(pawn* x, int i, int j)
{
	if (i < 0 || SIZE_FIELD <= i)
	{
		return 0;
	}

	if (j < 0 || SIZE_FIELD <= j)
	{
		return 0;
	}
	
	field[x->i][x->j] = NULL;
	x->i = i;
	x->j = j;
	field[i][j] = x;
	return 1;
}
int pawn_attack(pawn* x, pawn* y)
{
	if (x == NULL || y == NULL)
	{
		return 0;
	}
	if (pawn_isAttackable(x, y))
	{
		x->atk = 1;
		y->hp -= 1;
		if (y->hp == 0)
		{
			field[y->i][y->j] = NULL;
		}
		return 1;
	}
	else
	{
		return 0;
	}
}
int pawn_isAvailable(pawn* x)
{
	if (x != NULL)
	{
		if (x->hp > 0 && (x->mov == 0 || x->atk == 0))
		{
			return 1;
		}
	}
	return 0;
}
int pawn_isAttackable(pawn* x, pawn* y)
{
	if (x == NULL || y == NULL)
	{
		return 0;
	}

	if (x->atk == 0 && y->hp > 0 && x->num/SIZE_PAWN != y->num/SIZE_PAWN)
	{
		if (0
			|| (x->i == y->i + 0 && x->j == y->j + 1)
			|| (x->i == y->i - 0 && x->j == y->j - 1)
			|| (x->i == y->i + 1 && x->j == y->j + 0)
			|| (x->i == y->i - 1 && x->j == y->j - 0)
			)
		{
			return 1;	
		}
	}

	return 0;
}
int pawn_isMovable(pawn* x, int i, int j, int turn)
{
	int di, dj;
	if (x != NULL)
	{
		di = i - x->i;
		di = di > 0 ? di : -di;
		dj = j - x->j;
		dj = dj > 0 ? dj : -dj;
		if (di + dj <= 3)
		{
			pawn_setMovable(x, turn);
			return movable[i - x->i + 3][j - x->j + 3];
			// return movable[x->i - i + 3][x->j - j + 3];
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}
int pawn_isOpposite(pawn* x, int turn)
{
	if (x == NULL)
	{
		return -1;
	}

	if (x->num / SIZE_PAWN == turn%2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
void pawn_setMovable(pawn* x, int turn)
{
	int di, dj;
	int ci, cj;
	int rotate;
	int roop;
	int temp;
	#define AJST(x) (x+3)
	// _________ 03
	// ______ 12 13 14 
	// ___ 21 22 23 24 25
	//  30 31 32  + 34 35 36
	// ___ 41 42 43 44 45
	// ______ 52 53 54
	// _________ 63

	if (x == NULL)
	{
		return;
	}

	for (di = -3; di <= 3; ++di)
	{
		for (dj = -3; dj <= 3; ++dj)
		{
			movable[AJST(di)][AJST(dj)] = 0;
		}
	}
	movable[AJST(0)][AJST(0)] = 2;

	for (roop = 0; roop < 6; ++roop)
	{
		switch (roop)
		{
			case 0:
			{
				di = 0;
				dj = 1;
				break;
			}
			case 1:
			{
				di = 0;
				dj = 2;
				break;
			}
			case 2:
			{
				di = 0;
				dj = 3;
				break;
			}
			case 3:
			{
				di = 1;
				dj = 1;
				break;
			}
			case 4:
			{
				di = 1;
				dj = 2;
				break;
			}
			case 5:
			{
				di = 2;
				dj = 1;
				break;
			}
		}
		ci = di - 1;
		cj = dj - 1;

		for (rotate = 0; rotate < 4; ++rotate)
		{
			if (pawn_isInside(x->i + di, x->j + dj))
			{
				// コマがあるとき
				if (field[x->i+di][x->j+dj] != NULL)
				{
					// 自分のコマなら
					if (field[x->i+di][x->j+dj]->num / SIZE_PAWN == turn%2)
					{
						// 通過可能
						movable[AJST(di)][AJST(dj)] = 2;
					}
				}
				else
				{
					// 無いなら移動可能
					movable[AJST(di)][AJST(dj)] = 1;
				}
				
				// 敵コマによる移動不可判定
				if (di == 0)
				{
					if (movable[AJST(di)][AJST(cj)] == 0)
					{
						movable[AJST(di)][AJST(dj)] = 0;
					}
				}
				else if (dj == 0)
				{
					if (movable[AJST(ci)][AJST(dj)] == 0)
					{
						movable[AJST(di)][AJST(dj)] = 0;
					}
				}
				else
				{
					if (movable[AJST(ci)][AJST(dj)] == 0 && movable[AJST(di)][AJST(cj)] == 0)
					{
						movable[AJST(di)][AJST(dj)] = 0;
					}
				}
			}

			// +90度
			temp = di;
			di = dj;
			dj = -temp;
			temp = ci;
			ci = cj;
			cj = -temp;
		}
	}
}
void pawn_activate(void)
{
	int i;
	for (i = 0; i < SIZE_PAWN*2; ++i)
	{
		man[i].atk = 0;
		man[i].mov = 0;
	}
}
int pawn_isInside(int i, int j)
{
	if (0
		|| i < 0
		|| i >= SIZE_FIELD
		|| j < 0
		|| j >= SIZE_FIELD)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

void scr_disp_frame(void)
{
	int i, j;

	mvprintw(0, 0, "    ");
	attrset(A_UNDERLINE);
	for (j = 0; j < SIZE_FIELD; ++j)
	{
		printw(" %c ", 'A'+j);
	}
	attrset(0);
	printw("\n");

	for (i = 0; i < SIZE_FIELD; ++i)
	{
		printw("%2d |", i+1);
		if (i == SIZE_FIELD - 1)
		{
			attrset(A_UNDERLINE);
		}

		for (j = 0; j < SIZE_FIELD; ++j)
		{
			printw("+++");
		}
		
		if (i == SIZE_FIELD - 1)
		{
			attrset(A_UNDERLINE);
		}
		attrset(0);
		printw("|\n");
	}
}
void scr_disp_none(void)
{
	int i, j;

	for (i = 0; i < SIZE_FIELD; ++i)
	{
		if(i == SIZE_FIELD - 1)
		{
			attrset(A_UNDERLINE);
		}

		for (j = 0; j < SIZE_FIELD; ++j)
		{
			mvprintw(1+i, 4+j*3, "   ", i*SIZE_FIELD+j);
		}

		if(i == SIZE_FIELD - 1)
		{
			attrset(0);
		}
	}
}
void scr_set_status(int turn)
{
	int y_, x_;
	getyx(stdscr, y_, x_);
	move(SIZE_FIELD+2, 1);
	deleteln();
	insertln();
	printw("%sのターン", turn%2==0 ? "赤軍" : "青軍");
	move(y_, x_);
}
void scr_set_script(char* str1, char* str2)
{
	int y_, x_;
	getyx(stdscr, y_, x_);
	move(SIZE_FIELD+4, 1);
	deleteln();
	insertln();
	printw(str1);
	move(SIZE_FIELD+5, 1);
	deleteln();
	insertln();
	printw(str2);
	move(y_, x_);
}
void scr_disp_pawn_num(void)
{
	int i, j;

	for (i = 0; i < SIZE_FIELD; ++i)
	{
		if(i == SIZE_FIELD - 1)
		{
			attrset(A_UNDERLINE);
		}

		for (j = 0; j < SIZE_FIELD; ++j)
		{
			if (field[i][j])
			{
				mvprintw(1+i, 4+j*3, " %2d", field[i][j]->num);
			}
			else
			{
				mvprintw(1+i, 4+j*3, "   ");
			}
		}

		if(i == SIZE_FIELD - 1)
		{
			attrset(0);
		}
	}
}
void scr_over_owner(void)
{
	int i, j;
	char str[4];

	for (i = 0; i < SIZE_FIELD; ++i)
	{
		for (j = 0; j < SIZE_FIELD; ++j)
		{
			if(i == SIZE_FIELD - 1)
			{
				attron(A_UNDERLINE);
			}

			if (field[i][j])
			{
				move(1+i, 4+j*3);
				innstr(str, 3);
				mvprintw(1+i, 4+j*3, " ");
				if (field[i][j]->num < SIZE_PAWN)
				{
					attron(COLOR_PAIR(C_RED));
				}
				else
				{
					attron(COLOR_PAIR(C_BLUE));
				}
				if (field[i][j]->hp == 1)
				{
					attron(A_BOLD);
				}
				mvprintw(1+i, 4+j*3+1, "%2d", field[i][j]->num);
				attroff(COLOR_PAIR(C_RED));
				attroff(COLOR_PAIR(C_BLUE));
				attroff(A_BOLD);
			}
			else
			{
				mvprintw(1+i, 4+j*3, "   ");
			}
	
			if(i == SIZE_FIELD - 1)
			{
				attroff(A_UNDERLINE);
			}
		}
	}
}
void scr_over_move(pawn* x, int turn)
{
	int i, j;
	int y_, x_;
	int str[4];

	getyx(stdscr, y_, x_);
	// pawn_setMovable(x, turn);
	for (i = 0; i < SIZE_FIELD; ++i)
	{
		for (j = 0; j < SIZE_FIELD; ++j)
		{
			if(i == SIZE_FIELD - 1)
			{
				attron(A_UNDERLINE);
			}

			if (pawn_isMovable(x, i, j, turn))
			{
				move(1+i, 4+j*3);
				// innstr(str, 3);
				mvprintw(1+i, 4+j*3, " ");
				attron(COLOR_PAIR(C_YELLOW));
				if (field[i][j])
				{
					if (field[i][j]->hp == 1)
					{
						attron(A_BOLD);
					}
					mvprintw(1+i, 4+j*3+1, "%2d", field[i][j]->num);
				}
				else
				{
					mvprintw(1+i, 4+j*3+1, "  ");
				}
				attroff(COLOR_PAIR(C_YELLOW));
				attroff(A_BOLD);
			}
	
			if(i == SIZE_FIELD - 1)
			{
				attroff(A_UNDERLINE);
			}
		}
	}
	move(y_, x_);	
}
int scr_get_num(void)
{
	int get_num;
	int i;
	char buf[100];
	int y0, x0;


	for (i = 0; i < 100; ++i)
	{
		buf[i] = 0;
	}
	getyx(stdscr, y0, x0);
	echo();
	move(SIZE_FIELD+4, 2);

	getstr(buf);
	for (i = 0; i < 100; ++i)
	{
		if (!('0' <= buf[i] && buf[i] <= '9') && buf[i] != 0)
		{
			move(y0, x0);
			noecho();
			return -1;
		}
	}
	noecho();
	return get_num = atoi(buf);
}
void scr_animation(void)
{
	scr_keep_call(scr_disp_pawn_num);
	scr_keep_call(scr_over_owner);
	wrefresh(stdscr);
	usleep(ANIME_DELAY);
}
void scr_disp_progress(int trial, int lim_trial)
{
	mvprintw(SIZE_FIELD+2, 1, "progress: %6d / %6d", trial, lim_trial);
}

void grid_move(int i, int j)
{
	move(1+i, 4+j*3);
}
void grid_move_dir(int dir, int* i, int* j)
{
	switch (dir)
	{
		case DIR_UP:
			*i = *i > 0 ? *i-1 : SIZE_FIELD - 1;
			break;
		case DIR_DOWN:
			*i = *i < SIZE_FIELD - 1 ? *i+1 : 0;
			break;
		case DIR_LEFT:
			*j = *j > 0 ? *j-1 : SIZE_FIELD - 1;
			break;
		case DIR_RIGHT:
			*j = *j < SIZE_FIELD - 1 ? *j+1 : 0;
			break;
		default:
			break;
	}
	grid_move(*i, *j);
}
void scr_keep_call(void (*func)(void))
{
	int y_, x_;
	getyx(stdscr, y_, x_);
	func();
	move(y_, x_);	
}
void debug_print(int i, char* str)
{
	getyx(stdscr, y_debug, x_debug);
	mvprintw(i, SIZE_FIELD*3+10, str);
	move(y_debug, x_debug);
}
void debug_print_num(int i, char* str, int x)
{
	getyx(stdscr, y_debug, x_debug);
	mvprintw(i, SIZE_FIELD*3+8, "%s%d", str, x);
	move(y_debug, x_debug);
}

void ai0_control(int skip_anime)
{
	int n;
	for (n = SIZE_PAWN-1; n >= 0; --n)
	{
		// if (man[n].hp > 0)
		// {
		// 	if (pawn_isMovable(&man[n], man[n].i+1, man[n].j+1, 0) == 1)
		// 	{
		// 		pawn_move(&man[n], man[n].i+1, man[n].j+1);
		// 		if (!skip_anime)
		// 		{
		// 			scr_animation();
		// 		}
		// 	}

		// 	ai_attack_any(&man[n]);
		// }

		if (man[n].hp > 0)
		{
			ai_rand_move(&man[n]);

			ai_attack_any(&man[n]);
		}
	}
}
void ai1_control(int skip_anime)
{
	int n;

	for (n = SIZE_PAWN; n < SIZE_PAWN*2; ++n)
	{
		if (man[n].hp > 0)
		{
			ai_rand_move(&man[n]);

			ai_attack_any(&man[n]);
		}
	}
}
void ai_get_g(int *gi, int *gj)
{

}
void ai_attack_any(pawn* x)
{
	if (pawn_isInside(x->i, x->j-1))
	{
		pawn_attack(x, field[x->i][x->j-1]);
	}
	if (pawn_isInside(x->i-1, x->j))
	{
		pawn_attack(x, field[x->i-1][x->j]);
	}
	if (pawn_isInside(x->i, x->j+1))
	{
		pawn_attack(x, field[x->i][x->j+1]);
	}
	if (pawn_isInside(x->i+1, x->j))
	{
		pawn_attack(x, field[x->i+1][x->j]);
	}
}
void ai_rand_move(pawn* x)
{
	int ri, rj;
	int r_dir;

	switch (r_dir = rand()%4)
	{
		case DIR_UP:
		{
			ri = x->i-1;
			rj = x->j;
			break;
		}
		case DIR_DOWN:
		{
			ri = x->i+1;
			rj = x->j;
			break;
		}
		case DIR_LEFT:
		{
			ri = x->i;
			rj = x->j-1;
			break;
		}
		case DIR_RIGHT:
		{
			ri = x->i;
			rj = x->j+1;
			break;
		}
	}

	if (pawn_isMovable(x, ri, rj, 1) == 1)
	{
		pawn_move(x, ri, rj);
	}
}